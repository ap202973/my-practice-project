class Animal {
  String? name='Aashish';
  int? numberOfLegs=2;
  int? lifeSpan=60yrs;

  void display() {
    print("Animal name: $name.");
    print("Number of Legs: $numberOfLegs.");
    print("Life Span: $lifeSpan.");
  }
}
void main(){
    Animal ob= Animal();
 ob.display();
}