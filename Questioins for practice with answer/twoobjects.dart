class Animal{
    int? lifeSpan;
    String? name;

    void display(){
        print('Name:$name');
        print('Lifespan:$lifeSpan');
    }
}
void main(){
    Animal animal=Animal();
    Animal animal2=Animal();
    animal.lifeSpan=10;
    animal.name='Aashish';
    animal.display();
    animal2.lifeSpan=15;
    animal2.name='Lion';
    animal2.display();
}