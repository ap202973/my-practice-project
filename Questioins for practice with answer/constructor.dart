class Teacher{
    String? name;
    int? age;
    String? subject;
    double? salary;

    Teacher(String name,int age,String subject,double salary){
        this.name=name;
        this.age=age;
        this.subject=subject;
        this.salary=salary;
    }

    void display(){
        print('Name:${name}');
        print('Age:${age}');
        print('Subject:${subject}');
        print('Salary:${salary}');
    }
}
void main(){
    Teacher teacher1=Teacher('Apar',27,'Dart',50000);
    Teacher teacher2=Teacher('Aashish',31,'OS',50000);
    teacher1.display();
    teacher2.display();
}