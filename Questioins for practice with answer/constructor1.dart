class Patient{
    String? name;
    int? age;
    String? disease;

    //  Patient(this.name,this.age,[this.disease]);
    Patient({this.name,this.age,this.disease});
    void display(){
        print('Name:$name');
        print('Age:$age');
        print('Disease:$disease');
    }
    
}
void main(){
    // Patient patient=Patient('Aashish',16);
     Patient patient=Patient(name:'Aashish',age:16,disease:'cancerLastStageAlmostDead');
    patient.display();
}